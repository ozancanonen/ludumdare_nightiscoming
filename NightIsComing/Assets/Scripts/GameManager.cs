﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{
    public GameObject LoseScreen;
    public GameObject player;
    public GameObject EnemiesParentObject;
    public GameObject pauseMenuUI;

    [HideInInspector] public static bool gameIsPaused = false;
    [HideInInspector] public AudioManager am;
    [HideInInspector] public bool playerDead=false;

    void Start()
    {
        am = GetComponentInChildren<AudioManager>();
    }

void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }
    public void EndScene()
    {
        StartCoroutine(EndingScene());
    }

    IEnumerator EndingScene()
    {
        yield return new WaitForSeconds(1f);
        LoseScreen.SetActive(true);
    }
    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;
    }
}
