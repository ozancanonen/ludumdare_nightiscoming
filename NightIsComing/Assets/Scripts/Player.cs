﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class Player : MonoBehaviour
{
    public float Health=100;
    public float speed;
    public float jumpForce;
    public float timeBetweenTrail;
    public float checkRadius;
    public int extraJumpValue;

    public GameObject jumpParticle;
    public GameObject runParticle;
    public GameObject playerDeadParticles;
    public GameObject particleParentObject;

    public Slider HealthBar;
    public Transform groundCheck;
    public LayerMask whatIsGround;
    public string[] stepSounds;

    private float timeBetweenTrailValue;
    private float horizontalMove;
    private int extraJumps;
    private bool facingRight = false;
    private bool onGround;
    private bool spawnJumpParticles;

    private Animator anim;
    private Rigidbody2D rb;
    private GameManager gm;

    void Start()
    {
        timeBetweenTrailValue = timeBetweenTrail;
        extraJumps = extraJumpValue;
        rb = GetComponent<Rigidbody2D>();
        gm = GetComponentInParent<GameManager>();
        anim= GetComponentInChildren<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {

    }

    private void Update()
    {
        ManageMovement();
        spawningJumpParticles();
        spawningRunParticles();
        Attack();
    }

    void FixedUpdate()
    {
        ManageJumpingAndFallingAnim();
        FlipManager();
        
    }


    void ManageMovement()
    {
        onGround = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);
        horizontalMove = Input.GetAxisRaw("Horizontal") * speed;
        rb.velocity = new Vector2(horizontalMove, rb.velocity.y);
        anim.SetFloat("Speed", Mathf.Abs(horizontalMove));
        if (onGround == true)
        {
            anim.SetBool("IsFalling", false);
            anim.SetBool("IsJumping", false);
            extraJumps = extraJumpValue;
        }
        if (Input.GetKeyDown(KeyCode.W) && extraJumps > 0)
        {
            Instantiate(jumpParticle, groundCheck.position, Quaternion.identity);
            rb.velocity = Vector2.up * jumpForce;
            extraJumps--;
        }
        else if (Input.GetKeyDown(KeyCode.W) && extraJumps == 0 && onGround == true)
        {
            Instantiate(jumpParticle, groundCheck.position, Quaternion.identity);
            onGround = false;
            rb.velocity = Vector2.up * jumpForce;
        }
    }

    void ManageJumpingAndFallingAnim()
    {
        if (rb.velocity.y > 0)
        {
            anim.SetBool("IsJumping", true);
        }
        else
        {
            anim.SetBool("IsJumping", false);
        }

        if (rb.velocity.y < 0)
        {
            anim.SetBool("IsFalling", true);
        }
        else
        {
            anim.SetBool("IsFalling", false);
        }
    }

    void Attack()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            anim.SetTrigger("Attack");
        }
    }

    void spawningJumpParticles()
    {
        if (onGround)
        {
            if (spawnJumpParticles)
            {
                GameObject explosion = Instantiate(jumpParticle, groundCheck.position, Quaternion.identity);
                explosion.transform.parent = particleParentObject.transform;
                Destroy(explosion, 1);
                spawnJumpParticles = false;
            }
        }
        else
        {
            spawnJumpParticles = true;
        }
    }

    void spawningRunParticles()
    {
        if (onGround)
        {
             if (Mathf.Abs(horizontalMove) != 0)
             {
                if (timeBetweenTrail <= 0)
                {
                        GameObject explosion = Instantiate(runParticle, groundCheck.position, Quaternion.identity);
                        explosion.transform.parent = particleParentObject.transform;
                        Destroy(explosion, 1);
                        timeBetweenTrail = timeBetweenTrailValue;
                }
                else
                {
                        timeBetweenTrail -= Time.deltaTime;
                }
             }
            
        }
    }

    void FlipManager()
    {
        if (facingRight == false && horizontalMove > 0)
        {
            Flip();
        }
        else if (facingRight == true && horizontalMove < 0)
        {
            Flip();
        }
    }

        void Flip()
    {
        facingRight = !facingRight;
        Vector3 Scaler = gameObject.transform.localScale;
        Scaler.x *= -1;
        gameObject.transform.localScale = Scaler;
    }


    IEnumerator ifDeadLoseGame()
    {
        if (HealthBar.value <= 0)
        {
            gm.playerDead = true;
            yield return new WaitForSecondsRealtime(1f);
            //gm.am.Play("playerHit");
            GameObject explosion = Instantiate(playerDeadParticles, transform.position, Quaternion.identity);
            gm.EndScene();
            gameObject.SetActive(false);
        }
    }
}
